import { UsersReq } from '../models'


const FETCH_USERS = 'FETCH_USERS'


const fetchUsersAsync = (users) => {
    return {
        type: FETCH_USERS,
        users
    }
}

const fetchUsers = () => {
    return (dispatch) => {
        UsersReq.get('/')
            .then(res => {
                dispatch(fetchUsersAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_USERS, fetchUsers }
