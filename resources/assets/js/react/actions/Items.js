import { ItemsReq } from '../models'

const CREATE_NEW_ITEM = 'CREATE_NEW_ITEM'
const FETCH_ITEMS = 'FETCH_ITEMS'
const GET_ITEM = 'GET_ITEM'

const fetchItemsAsync = (items) => {
    return {
        type: FETCH_ITEMS,
        items
    }
}

const fetchItems = () => {
    return (dispatch) => {
        ItemsReq.get('/')
            .then(res => {
                dispatch(fetchItemsAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

const getItemAsync = (item) => {
    return {
        type: GET_ITEM,
        item
    }
}

const getItem = (id) => {
    return (dispatch) => {
        ItemsReq.get(`/${id}`)
            .then(res => {
                dispatch(getItemAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}


export { GET_ITEM, getItem, FETCH_ITEMS, fetchItems }
