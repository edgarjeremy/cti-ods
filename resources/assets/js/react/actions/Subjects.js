import { SubjectsReq } from '../models'

const FETCH_SUBJECTS = 'FETCH_SUBJECTS'

const fetchSubjectsAsync = (subjects) => {
    return {
        type: FETCH_SUBJECTS,
        subjects
    }
}

const fetchSubjects = () => {
    return (dispatch) => {
        SubjectsReq.get('/')
            .then(res => {
                dispatch(fetchSubjectsAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_SUBJECTS, fetchSubjects }
