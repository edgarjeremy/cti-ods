import { AuthorsReq } from '../models'

const FETCH_AUTHORS = 'FETCH_AUTHORS'

const fetchAuthorsAsync = (authors) => {
    return {
        type: FETCH_AUTHORS,
        authors
    }
}

const fetchAuthors = () => {
    return (dispatch) => {
        AuthorsReq.get('/')
            .then(res => {
                dispatch(fetchAuthorsAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_AUTHORS, fetchAuthors }
