import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchItemTypes, fetchCountries, fetchAuthors, fetchTags, fetchSubjects, fetchUsers } from '../../actions';
import FormElement from '../../components/items/Form';


const mapStateToProps = state => {
    return {
        countries: state.countries,
        itemTypes: state.itemTypes,
        authors: state.authors,
        tags: state.tags,
        subjects: state.subjects,
        users: state.users,
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        fetchItemTypes: fetchItemTypes,
        fetchCountries: fetchCountries,
        fetchAuthors: fetchAuthors,
        fetchTags: fetchTags,
        fetchSubjects: fetchSubjects,
        fetchUsers: fetchUsers
    }, dispatch)
}

const Form = connect(
    mapStateToProps,
    mapDispatchToProps
)(FormElement)

export default Form;
