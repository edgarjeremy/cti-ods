import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchItems  } from '../../actions';
import TableElement from '../../components/items/Table';


const mapStateToProps = state => {
    return {
        items: state.items
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        fetchItems: fetchItems
    }, dispatch)
}

const Table = connect(
    mapStateToProps,
    mapDispatchToProps
)(TableElement)

export default Table;
