export const ItemTypesReq = axios.create({
    baseURL: '/ODS-Modified/public/api/categories',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const ItemsReq = axios.create({
    baseURL: '/ODS-Modified/public/api/items',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const CountriesReq = axios.create({
    baseURL: '/ODS-Modified/public/api/countries',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const ItemCoverReq = (itemId) => {
    const ItemCover = axios.create({
        baseURL: `/ODS-Modified/public/api/items/${itemId}/cover`,
        timeout: 3600000,
        headers: {'Content-Type': 'application/json'}
    });

    return ItemCover
}

export const AuthorsReq = axios.create({
    baseURL: '/ODS-Modified/public/api/authors',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const TagsReq = axios.create({
    baseURL: '/ODS-Modified/public/api/tags',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const SubjectsReq = axios.create({
    baseURL: '/ODS-Modified/public/api/subjects',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const UsersReq = axios.create({
    baseURL: '/ODS-Modified/public/api/users',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const visibility = [
    { value: 1, name: 'All can see' },
    { value: 2, name: 'Only registered users' },
    { value: 4, name: 'Registered users only' },
]
