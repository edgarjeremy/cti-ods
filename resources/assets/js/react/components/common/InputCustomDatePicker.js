import React from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import InputCustomDatePicker from './InputCustomDatePicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../../../../sass/datepicker.scss';


export default class InputDatePicker extends React.Component {
    render() {
        return(
            <DatePicker customInput={
                <CustomDatePicker name={this.props.name} title={this.props.title} />} selected={this.props.date} onChange={this.props.onChange
                }
                dateFormat="DD MMMM YYYY" />
        )
    }
}
