import React from 'react';
import { propTypes, withFormsy } from 'formsy-react';


class Input extends React.Component {
    constructor(props) {
        super(props);
        this.changeValue = this.changeValue.bind(this);
    }

    changeValue(event) {
        // setValue() will set the value of the component, which in
        // turn will validate it and the rest of the form
        this.props.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value']);
    }

    render() {
        // Set a specific className based on the validation
        // state of this component. showRequired() is true
        // when the value is empty and the required prop is
        // passed to the input. showError() is true when the
        // value typed is invalid
        const emptyButError = (this.props.required && !this.props.isPristine() && !this.props.showError() && !this.props.isValidValue())
        const isError = (this.props.showError() && this.props.required)
        const isValid = (this.props.isValidValue() && this.props.required)
        const className = `form-group has-feedback ${this.props.showRequired() ? 'required' : ''} ${isError || emptyButError ? 'has-error' : ''} ${isValid ? 'has-success' : ''}`;

        // An error message is returned ONLY if the component is invalid
        // or the server has returned an error message
        const errorMessage = this.props.getErrorMessage();

        return (
            <div className={className}>
                <label htmlFor={this.props.name}>
                    {this.props.label}{this.props.required ? "*" : ""}
                </label>
                <input
                    onChange={this.changeValue}
                    name={this.props.name}
                    type={this.props.type || 'text'}
                    value={this.props.getValue() || ''}
                    className="form-control"
                    disabled={this.props.isFormDisabled()}
                    formNoValidate
                />

                {isValid ?
                        <i className="form-control-feedback glyphicon glyphicon-ok" data-fv-icon-for="fax"></i>: null
                }

                {isError || emptyButError ?
                        <i className="form-control-feedback glyphicon glyphicon-remove" data-fv-icon-for="fax"></i>: null
                }

                <span className='help-block'>
                    {emptyButError ?
                            (<i className="fa fa-warning">Value required!</i>) : null
                    }

                    {isError ?
                            (<i className="fa fa-warning">{errorMessage}</i>) : null
                    }
                </span>
            </div>
        );
    }
}

Input.propTypes = {
    ...propTypes
};

export default withFormsy(Input);
