import React from 'react';
import PropTypes from 'prop-types';

class CustomDatePicker extends React.Component {
    render() {
        return (
            <div className="form-group">
                <label htmlFor={this.props.name}>{this.props.label}</label>
                <input type="text" className="form-control" onClick={this.props.onClick} value={this.props.value} id={this.props.name} name={this.props.name} disabled={this.props.disabled} readOnly={true} />
            </div>
        )
    }
}

CustomDatePicker.propTypes = {
    onClick: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string
}

export default CustomDatePicker;
