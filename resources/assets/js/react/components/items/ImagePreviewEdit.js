import React from 'react';
import { propTypes, withFormsy } from 'formsy-react';

class InputImagePreview extends React.Component {
    constructor(props) {
        super(props)
        this.changeValue = this.changeValue.bind(this)
    }

    changeValue(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        if(file) {
            this.props.setValue(file)

            reader.onloadend = () => {
                this.props.setCover({
                    file: file,
                    imagePreviewUrl: reader.result
                })
            }

            reader.readAsDataURL(file)
        } else {
            this.props.resetValue()
            this.props.clearCover()
        }
    }

    render() {
        // Set a specific className based on the validation
        // state of this component. showRequired() is true
        // when the value is empty and the required prop is
        // passed to the input. showError() is true when the
        // value typed is invalid
        const emptyButError = (!this.props.isPristine() && !this.props.showError() && !this.props.isValidValue())
        const isError = (this.props.showError() && !this.props.isPristine())
        const isValid = (this.props.isValidValue() && !this.props.isPristine())
        const className = `form-group has-feedback ${this.props.showRequired() ? 'required' : ''} ${isError || emptyButError ? 'has-error' : ''} ${isValid ? 'has-success' : ''}`;

        // An error message is returned ONLY if the component is invalid
        // or the server has returned an error message
        const errorMessage = this.props.getErrorMessage();

        let imagePreview = null;
        if(this.props.isValid() && this.props.imagePreviewUrl) {
            imagePreview = (<img src={this.props.imagePreviewUrl} className="img-responsive" alt="image-review" />)
        } else if(this.props.originalCover) {
            imagePreview = (<img src={`/storage/${this.props.originalCover.download_path}`} className="img-responsive" alt="image-review" />)
        } else {
            imagePreview = (<div>No Image...</div>)
        }

        return(
            <div className={className}>
                <label htmlFor={this.props.name}>
                    {this.props.label}{this.props.required ? "*" : ""}
                </label>
                <input
                    onChange={this.changeValue}
                    name={this.props.name}
                    type="file"
                    className="form-control"
                    disabled={this.props.isFormDisabled()}
                    formNoValidate
                    ref={(input) => { this.inputFile = input;  }}
                    accept="image/x-png,image/jpeg"
                />

                {isValid ?
                        <i className="form-control-feedback glyphicon glyphicon-ok" data-fv-icon-for="fax"></i>: null
                }

                {isError || emptyButError ?
                        <i className="form-control-feedback glyphicon glyphicon-remove" data-fv-icon-for="fax"></i>: null
                }

                <span className='help-block'>
                    {emptyButError ?
                            (<i className="fa fa-warning">Value required!</i>) : null
                    }

                    {isError ?
                            (<i className="fa fa-warning">{errorMessage}</i>) : null
                    }
                </span>
                <hr />
                <div className="row">
                    <div className="col-md-6 col-lg-6 col-sm-6">
                        {imagePreview}
                    </div>
                </div>
                <hr />
            </div>
        )
    }
}

InputImagePreview.propTypes = {
    ...propTypes
}

export default withFormsy(InputImagePreview)
