import React from 'react';
import Panel from '../common/Panel.js'
import Form from '../../containers/items/Form';

class CreateItem extends React.Component {
    render() {
        return(
            <Panel title="Add New Item">
                <Form />
            </Panel>
        )
    }
}

export default CreateItem;
