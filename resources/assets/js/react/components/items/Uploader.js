import React, { Component } from 'react'

import FineUploaderTraditional from 'fine-uploader-wrappers'
// const baseURL = window.location.origin + "/ODS-Modified/public/";
const baseURL = document.querySelector("meta[name='base_url']").content;
console.log(baseURL);

const uploader = new FineUploaderTraditional({
    options: {
        autoUpload: false,
        maxConnections: 1,
        cors: {
            sendCredentials: true
        },
        chunking: {
            enabled: false
        },
        deleteFile: {
            enabled: false,
            endpoint: `${baseURL}/api/files`,
            customHeaders: {
                'X-Requested-With':'XMLHttpRequest',
                'X-CSRF-TOKEN': token
            }
        },
        request: {
            endpoint: `${baseURL}/api/files`,
            customHeaders: {
                'X-Requested-With':'XMLHttpRequest',
                'X-CSRF-TOKEN': token
            },
            paramsInBody: true,
            params: { item_id: 0 }
        },
        retry: {
            enableAuto: false
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'png', 'pdf', 'xlsx', 'xls', 'doc', 'docx', 'ppt', 'pptx'],
            itemLimit: 25,
            allowEmpty: false,
            sizeLimit: 26214400
        },
        callbacks: {
            onError: function(id, name, error, xhr) {
                notification({
                    title: 'File Uploader',
                    text: error,
                    icon: 'fa fa-times-circle',
                    type: 'error'
                })
            },
            onAllComplete: function(succeeded, failed) {
                notification({
                    title: 'File Uploader',
                    text: 'File(s) uploaded',
                    icon: 'fa fa-check-square-o',
                    type: 'success'
                })
            },
            onComplete: function(id, name, responseJSON, xhr) {
                uploader.methods.cancel(id)
            }
        }
    }
})

export default uploader;
