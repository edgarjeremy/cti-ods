import React from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import Formsy from 'formsy-react';
import { ItemsReq, ItemCoverReq, visibility } from '../../models';
import Input from '../common/Input';
import TextArea from '../common/TextArea';
import InputDatePicker from '../common/InputDatePicker';
import Select from '../common/Select';
import uploader from './Uploader';
import RowFile from './ListFile';
import SelectCountries from './SelectCountries';
import SelectAuthors from './SelectAuthors';
import SelectTags from './SelectTags';
import SelectUsers from './SelectUsers';
import InputImagePreview from './ImagePreviewEdit';

import Gallery from 'react-fine-uploader'
import 'react-fine-uploader/gallery/gallery.css'

import { addValidationRule  } from 'formsy-react';

addValidationRule('isFileSize', function (values, value, sizeInByte) {
    return !value || (value && value.size <= Number(sizeInByte))
});


class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            canSubmit: false,
            canUpload: false,
            isThereAnyFiles: true,
            disabled: false,
            selectedCountries: [],
            selectedAuthors: [],
            selectedTags: [],
            selectedUsers: [],
            docDate: moment(),
            itemId: 0,
            deletedFiles: [],
            cover: {
                file: '',
                imagePreviewUrl: ''
            }
        }

        this.submit = this.submit.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onValid = this.onValid.bind(this);
        this.onInvalid = this.onInvalid.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onChangeDocDate = this.onChangeDocDate.bind(this);
        this.setCountries = this.setCountries.bind(this);
        this.clearCountries = this.clearCountries.bind(this);
        this.setAuthors = this.setAuthors.bind(this);
        this.clearAuthors = this.clearAuthors.bind(this);
        this.setUsers = this.setUsers.bind(this);
        this.clearUsers = this.clearUsers.bind(this);
        this.setTags = this.setTags.bind(this);
        this.clearTags = this.clearTags.bind(this);
        this.addDeleteFile = this.addDeleteFile.bind(this);
        this.setCover = this.setCover.bind(this);
        this.clearCover = this.clearCover.bind(this);
    }

    setCover(cover) {
        console.log(cover.file.name)
        this.setState((prevState, props) => {
            return(update(prevState, {
                cover: {
                    $set: { ...cover }
                }
            }))
        })
    }

    clearTags() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedTags: {
                    $set: []
                }
            }))
        })
    }

    setTags(tags) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedTags: {
                    $set: tags
                }
            }))
        })
    }

    clearCover() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                cover: {
                    $set: {
                        file: '',
                        imagePreviewUrl: ''
                    }
                }
            }))
        })
    }

    clearCountries() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedCountries: {
                    $set: []
                }
            }))
        })
    }

    setCountries(countries) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedCountries: {
                    $set: countries
                }
            }))
        })
    }

    clearAuthors() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedAuthors: {
                    $set: []
                }
            }))
        })
    }

    setAuthors(countries) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedAuthors: {
                    $set: countries
                }
            }))
        })
    }

    setUsers(users) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedUsers: {
                    $set: users
                }
            }))
        })
    }

    clearUsers() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedUsers: {
                    $set: []
                }
            }))
        })
    }

    onChangeDocDate(date) {
        this.setState((prevState, props) => {
            return(update(prevState, { docDate: { $set: date } }))
        })
    }

    addDeleteFile(fileId) {
        this.setState((prevState, props) => {
            let filesStatus = prevState.deletedFiles.length+1 != props.item.files.length

            return(update(prevState, {
                deletedFiles: {
                    $push: [fileId]
                },
                isThereAnyFiles: {
                    $set: filesStatus
                }
            }))
        })
    }

    onChangeDocDate(date) {
        this.setState((prevState, props) => {
            return(update(prevState, { docDate: { $set: date } }))
        })
    }

    onSubmit(data) {
        //return true
        let mergedData = update(data, {
            $merge: { deleted_files: this.state.deletedFiles }
        })

        console.log(this.state.deletedFiles)
        console.log(mergedData)

        this.setState({disabled: true})
        ItemsReq.patch(`/${this.props.item.id}`, mergedData)
            .then((res) => {
                console.log(res)

                // cover uploader
                if(this.state.cover.file) {
                    let dataCover = new FormData()
                    dataCover.append('cover', this.state.cover.file)
                    dataCover.append('_method', 'PUT')
                    ItemCoverReq(res.data.id).post("", dataCover)
                        .then((resCover) => {
                            console.log(resCover)
                        })
                        .catch((errCover) => {
                            console.log(errCover)
                        })
                }
                // end cover uploader

                uploader.methods.setParams({item_id: res.data.id})
                uploader.methods.uploadStoredFiles()

                notification({
                    title: 'Update Item',
                    text: 'Item updated!',
                    icon: 'fa fa-check-square-o',
                    type: 'success'
                })

                this.props.getItem(this.props.item.id)
                this.setState({disabled: false})
                this.props.fetchTags()
            })
            .catch((err) => {
                console.log(err)

                notification({
                    title: 'Update Item',
                    text: 'Error when updating item, please refresh this page if error still occur',
                    icon: 'fa fa-times-circle',
                    type: 'error'
                })

                this.setState({disabled: false})
            })
    }

    onValid() {
        this.setState({ canSubmit: true })
    }

    onInvalid() {
        this.setState({ canSubmit: false })
    }

    submit() {
    }

    componentWillUpdate(nextProps) {
        if(nextProps.item.hasOwnProperty('document_date') && !this.props.item.hasOwnProperty('document_date')) {
            this.onChangeDocDate(moment(nextProps.item.document_date))
        }

        if(nextProps.item.hasOwnProperty('countries') && !this.props.item.hasOwnProperty('countries')) {
            this.setState((prevState, props) => {
                return(update(prevState, {
                    selectedCountries: {
                        $set: nextProps.item.countries.map((e) => {
                            return { value: e.id, label: e.name }
                        })
                    }
                }))
            })
        }

        if(nextProps.item.hasOwnProperty('authors') && !this.props.item.hasOwnProperty('authors')) {
            this.setState((prevState, props) => {
                return(update(prevState, {
                    selectedAuthors: {
                        $set: nextProps.item.authors.map((e) => {
                            return { value: e.id, label: e.name }
                        })
                    }
                }))
            })
        }

        if(nextProps.item.hasOwnProperty('tags') && !this.props.item.hasOwnProperty('tags')) {
            this.setState((prevState, props) => {
                return(update(prevState, {
                    selectedTags: {
                        $set: nextProps.item.tags.map((e) => {
                            return { value: e.id, label: e.name }
                        })
                    }
                }))
            })
        }

        if(nextProps.item.hasOwnProperty('visibility') && !this.props.item.hasOwnProperty('visibility')) {
            if(nextProps.item.visibility && nextProps.item.visibility.users) {
                this.setState((prevState, props) => {
                    return(update(prevState, {
                        selectedUsers: {
                            $set: nextProps.item.visibility.users.map((e) => {
                                return { value: e.id, label: `${e.first_name} ${e.last_name}` }
                            })
                        }
                    }))
                })
            }
        }
    }

    componentDidMount() {
        let itemId = document.getElementById('item-id').dataset.id
        this.props.getItem(itemId)
        this.props.fetchItemTypes()
        this.props.fetchCountries()
        this.props.fetchAuthors()
        this.props.fetchTags()
        this.props.fetchSubjects()
        this.props.fetchUsers()

        uploader.on('statusChange', (id, oldStatus, newStatus) => {
            this.setState((prevState, props) => {
                return update(prevState, {
                    canUpload: {
                        $set: uploader.methods.getUploads({ status: uploader.qq.status.SUBMITTED }).length > 0
                    }
                })
            })
        })

        uploader.on('onAllComplete', (id, oldStatus, newStatus) => {
            this.props.getItem(this.props.item.id)

            this.setState((prevState, props) => {
                return(update(prevState, {
                    deletedFiles: {
                        $set: []
                    },
                    isThereAnyFiles: {
                        $set: true
                    }
                }))
            })
        })
    }

    onChange(currentValues, isChanged) {
        //this.props.setNewConnection(this.refs.form.getModel())
    }

    mapInputs(inputs) {
        return {
            title: inputs.title,
            alternative_title: inputs.alternative_title || "",
            preview: inputs.preview || "",
            summary: inputs.summary || "",
            document_date: inputs.document_date,
            item_type_id: parseInt(inputs.item_type_id),
            published: parseInt(inputs.published),
            countries: inputs.countries || [],
            authors: inputs.authors || [],
            tags: inputs.tags || [],
            subject_id: parseInt(inputs.subject_id),
            visibility: inputs.visibility || 0,
            visibility_users: inputs.visibility_users || []
        };

    }

    visibilityUsersIsRequired() {
        return this.refs.form && parseInt(this.refs.form.getModel()['visibility']) > 2
    }

    render() {
        return (
            <Formsy ref="form" onSubmit={this.onSubmit} onValid={this.onValid} onInvalid={this.onInvalid} mapping={this.mapInputs} onChange={this.onChange} disabled={this.state.disabled}>
                <Input name="title" label="Title" required={true} value={this.props.item.title} />
                <Input name="alternative_title" label="Alternative Title" value={this.props.item.alternative_title} />
                <TextArea name="preview" label="Preview" value={this.props.item.preview} />
                <TextArea name="summary" label="Summary" value={this.props.item.summary} />
                <InputImagePreview name="cover" label="Cover Image" validations="isFileSize:2097152" validationError="Max file size is 2MB!" setCover={this.setCover} {...this.state.cover} clearCover={this.clearCover} originalCover={this.props.item.cover} required={false} />
                <InputDatePicker name="document_date" label="Document date" date={this.state.docDate} onChange={this.onChangeDocDate} value={this.props.item.document_date} />
                <SelectCountries name="countries" label="Countries" options={this.props.countries.map((country,idx) => { return { value: country.id, label: country.name } })} setCountries={this.setCountries} value={this.state.selectedCountries}/>
                <SelectAuthors name="authors" label="Authors" options={this.props.authors.map((authors,idx) => { return { value: authors.id, label: authors.name } })} setAuthors={this.setAuthors} value={this.state.selectedAuthors}/>
                <Select name="item_type_id" label="Category" options={this.props.itemTypes.map((itemType) => { return {value: itemType.id, name: itemType.name} })}  value={this.props.item.item_type_id} />
                <Select name="subject_id" label="Subject" options={this.props.subjects.map((subject) => { return {value: subject.id, name: subject.name} })} value={this.props.item.subject_id}  />
                <SelectTags name="tags" label="Tags" options={this.props.tags.map((tags,idx) => { return { value: tags.id, label: tags.name } })} setTags={this.setTags} value={this.state.selectedTags}/>
                <Select name="published" label="Publish?" options={[{value: 1, name: 'Yes'}, {value: 0, name: 'No'}]} required={true} value={`${this.props.item.published}`} />
                <hr />
                <Select name="visibility" label="Set Item Visibility" options={visibility} required={true} value={this.props.item.visibility ? this.props.item.visibility.type : 0} />
                <SelectUsers name="visibility_users" label="Visibility Users" options={this.props.users.filter(user => !user.current_user).map((user,idx) => { return { value: user.id, label: `${user.first_name} ${user.last_name}` } })} setUsers={this.setUsers} value={this.state.selectedUsers} required={this.visibilityUsersIsRequired()} disabled={!this.visibilityUsersIsRequired()}/>
                <hr />
                <Gallery uploader={ uploader } disabled={this.state.disabled} />
                <hr />

                {this.props.item.hasOwnProperty('files') ?
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>File Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.item.files.map((file, idx) => {
                                    return <RowFile idx={idx} key={file.file_id} {...file} addDeleteFile={this.addDeleteFile} />
                                })}
                            </tbody>
                        </table>
                        :
                        null
                }

                <hr />

                {!this.state.disabled ?
                        <button type="submit" className="btn btn-success btn-lg" disabled={!(this.state.canSubmit && (this.state.canUpload || this.state.isThereAnyFiles))}>Submit!</button>
                        :
                        <button type="button" className="btn btn-default btn-lg" disabled={true}>
                            <i className="fa fa-gear fa-spin"></i> Submitting...
                        </button>
                }

            </Formsy>
        );
    }
}

Form.propTypes = {
    item: PropTypes.object.isRequired,
    countries: PropTypes.array.isRequired,
    getItem: PropTypes.func.isRequired,
    itemTypes: PropTypes.array.isRequired,
    subjects: PropTypes.array.isRequired,
    fetchItemTypes: PropTypes.func.isRequired,
    fetchCountries: PropTypes.func.isRequired,
    fetchSubjects: PropTypes.func.isRequired
}

export default Form;
