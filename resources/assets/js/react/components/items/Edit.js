import React from 'react';
import Panel from '../common/Panel.js'
import FormEdit from '../../containers/items/FormEdit';


class EditItem extends React.Component {
    render() {
        return(
            <Panel title="Edit Item">
                <FormEdit />
            </Panel>
        )
    }
}

export default EditItem;
