import React from 'react';

export default class ListFile extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            show: true
        }

        this.toggleList = this.toggleList.bind(this);
        this.baseURL = document.head.querySelector("meta[name='base_url']").content;
    }

    toggleList() {
        this.setState((prevState, props) => {
            props.addDeleteFile(props.id)
            return { show: !prevState.show }
        })
    }

    render() {
        return(
            this.state.show?
            <tr>
                <td>{this.props.idx+1}</td>
                <td>
                    <a href={`${this.baseURL}/storage/${this.props.download_path}`} target="_blank">{this.props.filename}</a>
                </td>
                <td>
                    <a href={`${this.baseURL}/storage/${this.props.download_path}`} target="_blank"><i className="fa fa-eye" aria-hidden="true"></i></a>
                    { ' ' }
                    <a href="#" onClick={(e) => { e.preventDefault(); this.toggleList() }}><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            :
            null
        )
    }
}
