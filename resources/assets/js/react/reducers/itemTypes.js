import { FETCH_ITEM_TYPES } from '../actions'

const itemTypes = (state=[], action) => {
    switch(action.type) {
        case FETCH_ITEM_TYPES:
            return action.itemTypes
        default:
            return state
    }
}

export default itemTypes
