import { combineReducers  } from 'redux'
import itemTypes from './itemTypes'
import { items, item } from './Item'
import { countries } from './Countries'
import { authors } from './Authors'
import { tags } from './Tags'
import { subjects } from './Subjects'
import { users } from './Users'

const reducers = combineReducers({
    itemTypes,
    items,
    item,
    countries,
    authors,
    tags,
    subjects,
    users
})

export default reducers
