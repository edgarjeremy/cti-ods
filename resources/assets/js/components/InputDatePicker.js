import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../../sass/datepicker.scss';

class CustomDatePicker extends Component {
    render() {
        return (
            <div className="form-group">
                <label htmlFor={this.props.identity}>{this.props.label}</label>
                <input type="text" className="form-control" onClick={this.props.onClick} value={this.props.value} id={this.props.identity} name={this.props.identity} readOnly={true} />
            </div>
        )
    }
}

export default class InputDatePicker extends Component {
    render() {
        return(
            <DatePicker customInput={<CustomDatePicker identity={this.props.identity} label={this.props.label} />} selected={this.props.date} onChange={this.props.onChange} dateFormat="DD MMMM YYYY" />
        )
    }
}
