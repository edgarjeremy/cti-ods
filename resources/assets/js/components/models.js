export const ItemTypes = axios.create({
    baseURL: '/api/item/types',
    timeout: 3600000,
    headers: {'content-type': 'application/json'}
});

export const Items = axios.create({
    baseURL: '/api/items',
    timeout: 3600000,
    headers: {'content-type': 'application/json'}
});
