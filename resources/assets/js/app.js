let token = document.head.querySelector('meta[name="csrf-token"]');
let baseURL = document.head.querySelector('meta[name="base_url"');
window.baseURL = baseURL.content;
window.token = token.content;



window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.withCredentials = true

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

$("#trigger-notif").on("click", function () {
    $.ajax({
        url: `${window.baseURL}/api/log_user_notification`,
        method: "get",
        headers: {
            'x-csrf-token': window.token
        },
        success: function(data){
            var list = "";
            data.forEach(function(item,i){
                list += "<li class='notif-"+((item.read) ? "read" : "unread")+"'><a class='notif-links' data-id='"+item.id+"' href='"+window.baseURL+"/admin/items/"+item.log_item.item.id+"'><i class='fa fa-file fa-lg'></i>&nbsp;&nbsp;"+item.log_item.item.user.first_name+" "+get_action(item.log_item.submit_action)+" "+item.log_item.item.title+"</a></li>";
            });
            $(".notif-rows").html("").append(list);
        }
    });
});

$("#notification-wrapper").on("click", ".notif-links", function(e){
    var id = $(this).data().id;
    var url = $(this).prop("href");
    $.ajax({
        url: `${window.baseURL}/api/flag_read/${id}`,
        method: "get",
        headers: {
            'x-csrf-token': window.token
        },
        success: function(res){
            window.location.replace(url);
        }
    });
    e.preventDefault();
    return false;
});

function get_action(submit_action) {
    switch(submit_action) {
        case 1:
            return "uploaded";
            break;
        case 2:
            return "modified";
            break;
        case 3:
            return "deleted";
            break;
    }
}