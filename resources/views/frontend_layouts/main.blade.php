<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="dim4S" />


	<!-- Stylesheets
	============================================= -->
    <link rel="shortcut icon" href="images/new_ctiff_favicon.ico" type="image/x-icon">
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/magnific-popup.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/responsive.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Official Documents System - CTI-CFF | Coral Triangle Initiative on Coral Reefs Fisheries and Food Security</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		@include('frontend_layouts/_header')

		<section id="slider" class="slider-parallax dark full-screen" style="background: url({{asset('frontend-theme/images/landing/bg-cticff.jpg')}}) center;">

			<div class="slider-parallax-inner">

				<div class="container clearfix">

					<div class="vertical-middle">

						<div class="heading-block center nobottomborder">
							<h1 data-animate="fadeInUp">Search &amp; Find CTI-CFF Official Documents Here!</h1>
							<span data-animate="fadeInUp" data-delay="300">Coral Triangle Initiative on Coral Reefs Fisheries and Food Security <strong>Official Documents</strong> System</span>
						</div>
						@include('frontend_layouts/_search_form')
					</div>

					<a href="#" data-scrollto="#section-features" class="one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div id="section-features" class="heading-block title-center page-section">
						<h2>CTI-CFF Recent Documents</h2>
						<span>Preview and download our documents here</span>
					</div>

					<div class="clear"></div>

                    @include('frontend_layouts/_slider')

				</div>
			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">


			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2017 All Rights Reserved by <br>Coral Triangle Initiative on Coral Reefs Fisheries and Food Security.<br>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="https://web.facebook.com/CTICFF/" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="https://twitter.com/CTICFF" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="https://instagram.com/CTICFF" class="social-icon si-small si-borderless si-instagram">
								<i class="icon-instagram"></i>
								<i class="icon-instagram"></i>
							</a>

							<a href="https://www.youtube.com/user/coraltrianglevideo/videos" class="social-icon si-small si-borderless si-youtube">
								<i class="icon-youtube"></i>
								<i class="icon-youtube"></i>
							</a>
							
							<a href="http://coraltriangleinitiative.org" class="social-icon si-small si-borderless si-wikipedia">
								<i class="icon-wikipedia"></i>
								<i class="icon-wikipedia"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@cticff.org <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 026 <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 027 
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
    <script type="text/javascript" src="{{asset('frontend-theme/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend-theme/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
    <script type="text/javascript" src="{{asset('frontend-theme/js/functions.js')}}"></script>

	<script type="text/javascript">
		$(function() {
			$( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
		});
	</script>

</body>
</html>
