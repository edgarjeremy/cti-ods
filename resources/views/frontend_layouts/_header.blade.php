<header id="header" class="transparent-header{{Request::is(['search', 'book-details/*']) ? '-cti' : ''}} page-section dark" data-sticky-class="not-dark">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger">
                <i class="icon-reorder"></i>
            </div>


            <div id="logo">
                <a href="{{route('home')}}" class="standard-logo" data-dark-logo="{{asset('frontend-theme/images/logo_cti_dark.png')}}"><img src="{{asset('frontend-theme/images/logo_cti.png')}}" alt="CTI-CFF Logo"></a>
                <a href="{{route('home')}}" class="retina-logo" data-dark-logo="{{asset('frontend-theme/images/logo_cti_dark@2x.png')}}"><img src="{{asset('frontend-theme/images/logo_cti@2x.png')}}" alt="CTI-CFF Logo"></a>
            </div>


            <nav id="primary-menu">

                <ul>
                    <li>
                        <a href="{{ route('home') }}">
                            <div>Home</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('home', ['#content']) }}">
                            <div>Recent Documents</div>
                        </a>
                    </li>
                    @if (!Auth::check())
                    <li>
                        <a href="{{ route('login') }}">
                            <div>Login</div>
                        </a>
                    </li>
                    @else
                    <li>
                        <a href="#"><div>My Account</div></a>
                        <ul>
                            <li>
                                <a href="{{ route('dashboard') }}">
                                    <div>
                                        <i class="icon-dashboard"></i>Dashboard
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" id="btn-logout">
                                    <div>
                                        <i class="icon-line2-logout"></i>Logout
                                    </div>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif

                </ul>

            </nav>

        </div>

    </div>

</header>
