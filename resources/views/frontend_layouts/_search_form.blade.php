<form action="{{route('search')}}" method="get" role="form" class="landing-wide-form clearfix">
	<div class="col_four_fifth nobottommargin">
		<div class="col_one_fifth nobottommargin">
			<div class="form-group">
				<select class="form-control input-lg not-dark" name="c" id="c" value="{{ $c }}">
					<option value="0"  {{ $c == 0 ? "selected": "" }}>All category</option>
					@foreach($categories as $category)
					<option value="{{$category->id}}" {{ $c == "$category->id" ? "selected" : "" }}>{{$category->name}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col_one_fifth nobottommargin">
			<div class="form-group">
				<select class="form-control input-lg not-dark" name="s" id="s" value="{{ $s }}">
					<option value="0"  {{ $s == 0 ? "selected": "" }}>All Subject</option>
					@foreach($subjects as $subject)
					<option value="{{$subject->id}}" {{ $s == "$subject->id" ? "selected" : "" }}>{{$subject->name}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col_three_fifth col_last nobottommargin">
			<input type="text" id="q" name="q" class="form-control input-lg not-dark" value="{{ $q }}" placeholder="Keywords">
		</div>
	</div>
	<div class="col_one_fifth col_last nobottommargin">
		<button class="btn btn-lg btn-success btn-block btn-round nomargin" value="submit" type="submit" style="">SEARCH &nbsp;<i class="icon-search2"></i></button>
	</div>
</form>
