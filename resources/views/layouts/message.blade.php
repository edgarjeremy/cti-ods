<div class="row">
    <article class="col-sm-12">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert {{ 'alert-' .$msg }} fade in">
                    <button class="close" data-dismiss="alert">
                        ×
                    </button>
                    @if($msg == "danger")
                        <i class="fa-fw fa fa-times"></i>
                    @elseif($msg == "warning")
                        <i class="fa-fw fa fa-warning"></i>
                    @elseif($msg == "success")
                        <i class="fa-fw fa fa-check"></i>
                    @elseif($msg == "info")
                        <i class="fa-fw fa fa-info"></i>
                    @endif

                    @if($msg == "danger")
                        <strong>Warning!</strong> {{ Session::get('alert-' . $msg) }}
                     @else
                         <strong>{{ lcfirst($msg) }}!</strong> {{ Session::get('alert-' . $msg) }}
                    @endif

                </div>
            @endif
        @endforeach
    </article>
</div>
