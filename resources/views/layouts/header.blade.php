<!-- #HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <a href="{{ route('home') }}">
            <span id="logo"> <img src="{{asset('theme/img/logo.png')}}" alt="SmartAdmin"> </span>
        </a>
        <!-- END LOGO PLACEHOLDER -->

        <!-- END AJAX-DROPDOWN -->
    </div>


    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- Notification  -->
        <ul id="notification-wrapper" class="header-dropdown-list padding-5">
            <li class="">
                <a href="#" id="trigger-notif" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <i class="fa fa-lg fa-fw fa-bell"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <h1><i class="fa fa-bell fa-lg"></i> Notifications</h1>
                    <div class="notif-rows">
                    </div>
                </ul>
            </li>
        </ul>

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <img src="{{asset('img/default-profile.jpg')}}" alt="John Doe" class="online" />
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="{{route('setting')}}" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{route('profile')}}" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}"
                        class="padding-10 padding-top-0 padding-bottom-0"
                        onclick="event.preventDefault();
                        $('#logout-form').submit();">
                            <i class="fa fa-sign-out fa-lg"></i> Logout </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
