<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">CTI CFF ODS <span class="hidden-xs"> - an Official Documents System platform by CTI CFF</span> © 2017</span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
