<!-- #NAVIGATION -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it -->

            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <img src="{{asset('img/default-profile.jpg')}}" alt="me" class="online" />
                <span>
                    {{Auth::user()->full_name()}}
                </span>
            </a>

        </span>
    </div>
    <!-- end user info -->

    <nav>
        <!--
        NOTE: Notice the gaps after each icon usage <i></i>..
        Please note that these links work a bit different than
        traditional href="" links. See documentation for details.
        -->

        <ul>
            <li class="{{ \Request::is('admin/dashboard') ? 'active' : ''  }}">
                <a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
                <ul>
                    <li class="{{ \Request::is('admin/dashboard') ? 'active' : ''  }}">
                        <a href="{{ route('dashboard') }}" title="Dashboard"><span class="menu-item-parent">Main Dashboard</span></a>
                    </li>
                </ul>
            </li>
            <li class="{{ \Request::is(['admin/users', 'admin/users/*', 'admin/roles', 'admin/roles/*']) ? 'active' : ''  }}">
                <a href="#"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Users & Roles</span></a>
                <ul>
                    @if(!Auth::user()->is_role('contributor'))
                    <li class="{{ \Request::is('admin/users/create') ? 'active' : '' }}">
                        <a href="{{ route('users.create') }}">Add User</a>
                    </li>
                    @endif
                    <li class="{{ \Request::is('admin/users') ? 'active' : '' }}">
                        <a href="{{ route('users.index') }}" title="Dashboard"><span class="menu-item-parent">List Users</span></a>
                    </li>
                    <li class="{{ \Request::is('admin/roles') ? 'active' : '' }}">
                        <a href="{{ route('roles.index') }}" title="Dashboard"><span class="menu-item-parent">Roles</span></a>
                    </li>
                </ul>
            </li>
            <li class="{{ \Request::is(['admin/items', 'admin/items/*', 'admin/countries', 'admin/countries/*', 'admin/types', 'admin/types/*', 'admin/authors', 'admin/authors/*', 'admin/subjects', 'admin/subjects/*']) ? 'active' : ''  }}">
                <a href="#"><i class="fa fa-lg fa-fw fa-list"></i> <span class="menu-item-parent">Items & Attributes</span></a>
                <ul>
                    <li class="{{ \Request::is('admin/items/create') ? 'active' : '' }}">
                        <a href="{{ route('items.create') }}">Add Item</a>
                    </li>
                    <li class="{{ \Request::is([ 'admin/items', 'admin/items/*' ]) ? 'active' : '' }}">
                        <a href="{{ route('items.index') }}" title="Dashboard"><span class="menu-item-parent">List Items</span></a>
                    </li>
                    <li class="{{ \Request::is([ 'admin/countries', 'admin/countries/*' ]) ? 'active' : '' }}">
                        <a href="{{ route('countries.index') }}" title="Dashboard"><span class="menu-item-parent">List Countries</span></a>
                    </li>
                    <li class="{{ \Request::is([ 'admin/categories', 'admin/categories/*' ]) ? 'active' : '' }}">
                        <a href="{{ route('categories.index') }}" title="Dashboard"><span class="menu-item-parent">List Categories</span></a>
                    </li>
                    <li class="{{ \Request::is([ 'admin/authors', 'admin/authors/*' ]) ? 'active' : '' }}">
                        <a href="{{ route('authors.index') }}" title="Dashboard"><span class="menu-item-parent">List Authors</span></a>
                    </li>
                    <li class="{{ \Request::is([ 'admin/subjects', 'admin/subjects/*' ]) ? 'active' : '' }}">
                        <a href="{{ route('subjects.index') }}" title="Dashboard"><span class="menu-item-parent">List Subjects</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>


    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>

</aside>
<!-- END NAVIGATION -->
