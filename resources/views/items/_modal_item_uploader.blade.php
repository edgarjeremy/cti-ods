<div id="modal-item-uploader" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Upload Item's file</h4>
      </div>
      <div class="modal-body">
          <form action="/target-url" id="form-item-uploader">
              <div class="form-group">
                  <label for="item-file">File</label>
                  <div class="input-group">
                      <input class="form-control" id="file-name" name="file-name" type="text" readonly>
                      <div class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btn-add-file" name="btn-add-file">
                              select file
                          </button>
                      </div>
                  </div>
              </div>

              <div class="form-group">
                  <label for="item-file-explanation">Description</label>
                  <textarea name="item-file-description" id="item-file-description" class="form-control"></textarea>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn-upload-item-file" class="btn btn-primary">Upload</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
