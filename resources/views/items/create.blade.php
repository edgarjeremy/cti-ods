@extends('layouts.main')


@section('customstyle')
@endsection

@section('content')
<div id="app-root"></div>
@endsection

@section('customjs')
<script src="{{asset('vendors/moment/moment.js')}}"></script>
<script src="{{asset('js/items.js')}}"></script>
@endsection
