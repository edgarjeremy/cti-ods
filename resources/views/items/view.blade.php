@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Item Details</div>
            <div class="panel-body">
                <div class="panel panel-primary panel-item-files">
                    <div class="panel-heading">
                        Title
                    </div>
                    <div class="panel-body">
                        {{$item->title}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Alternative Title
                    </div>
                    <div class="panel-body">
                        {{$item->alternative_title}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Preview
                    </div>
                    <div class="panel-body">
                        {{$item->preview}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Summary
                    </div>
                    <div class="panel-body">
                        {{$item->summary}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Uploader
                    </div>
                    <div class="panel-body">
                        {{$item->uploaded_by->full_name()}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Cover Image
                    </div>
                    <div class="panel-body">
                        @if($item->cover)
                        <img src="{{asset('storage/'.$item->cover->download_path)}}" className="img-responsive" alt="image-review" />
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Category
                    </div>
                    <div class="panel-body">
                        {{$item->item_type ? $item->item_type->name : ''}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Subject
                    </div>
                    <div class="panel-body">
                        {{$item->subject ? $item->subject->name : ''}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Authors
                    </div>
                    <div class="panel-body">
                        <ul class="list">
                            @foreach($item->authors as $author)
                            <li>{{ $author->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Tags
                    </div>
                    <div class="panel-body">
                            @foreach($item->tags as $tag)
                            <div class="label label-success">{{ $tag->name }}</div>
                            @endforeach
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Document Date
                    </div>
                    <div class="panel-body">
                        {{$item->document_date}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-files" id="item-files-list">
                    <div class="panel-heading">
                        Item Files
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                @foreach ($item->files as $file)
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <a target="_blank" href="{{URL::to('/storage/'.$file->download_path)}}">{{$file->filename}}</a>
                                        </div>
                                        <div class="panel-body">
                                            @if($file->description)
                                                {{$file->description}}
                                            @else
                                                no description...
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('items/_modal_item_uploader')
@endsection
