@extends('layouts.main')

@section('content')
<div class="">

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Alternative Title</th>
                <th>Category</th>
                <th>Created At</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            @if(count($items) > 0)
                @foreach($items as $item)
                    <tr>
                        <td>{{$item->title}}</td>
                        <td>{{$item->alternative_title}}</td>
                        <td>{{(!empty($item->item_type)) ? $item->item_type->name : ""}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            <a href="{{route('items.show', [$item->id])}}" class="btn btn-primary">View</a>
                            <a href="{{route('items.edit', [$item->id])}}" class="btn btn-primary">Edit</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    {{$items->links()}}
</div>
@include('items/_modal_item_uploader')
@endsection
