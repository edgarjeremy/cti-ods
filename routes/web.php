<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Item;
use App\ItemType;
use App\Subject;
use App\Models\Node;


Route::get('/', function (Request $request) {
    return view('frontend_layouts.main', [
        'search_result' => [],
        'items' => Item::published()->orderByDesc('document_date')->published()->paginate(20),
        'categories' => ItemType::all(),
        'subjects' => Subject::all(),
        'c' => $request['c'] ? $request['c'] : 0,
        's' => $request['s'] ? $request['s'] : 0,
        'q' => $request['q'] ? $request['q'] : ""
    ]);
})->name('home');

Route::get('/search', function (Request $request) {
    Log::Info($request);

    //$validator = Validator::make($request->all(), [
        //'c' => 'required|integer|min:1|exists:item_types,id',
        //'s' => 'required|integer|min:1|exists:subjects,id',
        //'q' => 'sometimes|string',
    //]);

    $validatorQ = Validator::make($request->all(), [
        'q' => 'required|string'
    ]);

    $validatorC = Validator::make($request->all(), [
        'c' => 'required|integer|min:1|exists:item_types,id'
    ]);

    $validatorS = Validator::make($request->all(), [
        's' => 'required|integer|min:1|exists:subjects,id'
    ]);

    $search_result = [];

    if($validatorQ->fails() && $validatorC->fails() && $validatorS->fails()) {
        $search_result = Item::published();
    } else {
        if(!$validatorC->fails()) {
            $search_result = $search_result ? $search_result->category($request['c']) : Item::category($request['c']);
        }

        if(!$validatorS->fails()) {
            $search_result = $search_result ? $search_result->findSubject($request['s']) : Item::findSubject($request['s']);
        }

        if(!$validatorQ->fails()) {
            $search_result = $search_result ? $search_result->title($request['q']) : Item::title($request['q']);
        }
    }

    return view('frontend_layouts.search', [
        'search_result' => $search_result ? $search_result->paginate(12)->appends($request->except('page')) : $search_result,
        'items' => Item::orderByDesc('document_date')->limit(4)->get(),
        'categories' => ItemType::all(),
        'subjects' => Subject::all(),
        'c' => $request['c'] ? $request['c'] : 0,
        's' => $request['s'] ? $request['s'] : 0,
        'q' => $request['q'] ? $request['q'] : ""
    ]);
})->name('search');


Route::get('/book-details/{id}', function ($id) {
    $item = Item::published()->findOrFail($id);
    if(Auth::check() && $item->uploaded_by->id == Auth::user()->id) {
        // do nothing, just continue
    } else if($item->visibility && $item->visibility->type > 1) {
        if(Auth::check()) {
            Log::info(Auth::check());
            Log::info($item->visibility->users);
            if($item->visibility->type == 3) {
                Log::info('type 3');
                $user = $item->visibility->users()->where('user_id', Auth::user()->id)->get();
                Log::info($user);
                Log::info($user->count());
                if($user->count() >= 1) {
                    Log::info('abort');
                    return abort(404);
                }
            }

            if($item->visibility->type == 4) {
                Log::info('type 4');
                $item->visibility->users()->where('user_id', Auth::user()->id)->firstOrFail();
            }
        } else {
            return redirect()->route('login', ['next' => URL::to('/book-details/'.$id)]);
        }
    }

    return view('frontend_layouts.book_details', [
        'item' => $item,
        'items' => Item::published()->paginate(8),
        'related_items' => Item::relatedItem($item) ? Item::relatedItem($item)->paginate(2) : Item::relatedItem($item)
    ]);
})->name('book_details');

Auth::routes();

Route::get('current_user', ['uses'=> 'ItemsController@current_user']);
Route::get('profile_photo/{id}', ['uses' => 'UsersController@show_profile']);
Route::get('items/{total}/{offset}', ['uses'=> 'ItemFilesController@get_items']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('users/{id}/delete', 'UsersController@delete')->name('users.delete');
    Route::resource('users', 'UsersController');
    Route::resource('roles', 'RolesController');
    Route::resource('items', 'ItemsController', ['only' => ['index', 'show', 'edit', 'create']]);
    Route::get('countries/{id}/delete', 'CountriesController@delete')->name('countries.delete');
    Route::resource('countries', 'CountriesController');

    Route::get('categories/{id}/delete', 'ItemTypesController@delete')->name('categories.delete');
    Route::resource('categories', 'ItemTypesController');

    Route::get('authors/{id}/delete', 'AuthorsController@delete')->name('authors.delete');
    Route::resource('authors', 'AuthorsController');

    Route::get('subject/{id}/delete', 'SubjectsController@delete')->name('subjects.delete');
    Route::resource('subjects', 'SubjectsController');

    Route::get('profile', 'ProfileController@edit')->name('profile');
    Route::post('profile', 'ProfileController@update')->name('profile');
    Route::get('setting', 'SettingController@edit')->name('setting');
    Route::post('setting', 'SettingController@update')->name('setting');
});

Route::redirect('/admin', '/admin/dashboard', 301);
