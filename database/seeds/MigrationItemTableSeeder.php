<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\ItemType;
use App\Country;
use App\Author;
use App\ItemCover;
use App\Models\Node;
use App\ItemFiles;
use App\Subject;
use App\User;
require_once 'vendor/autoload.php';
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class MigrationItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$nodes = Node::where('type', 'document')->where('nid', 9101)->limit(1)->get();
        $nodes = Node::where('type', 'document')->orderBy('nid', 'asc')->get();
        $loop = 1;
        foreach($nodes as $node) {
            Log::info($loop."-> nid: ".$node->nid." -> ".$node->title);
            $loop+=1;
            $item = Item::where('title', $node->title)->first();

            /*$item = Item::create([
                'title' => $node->title,
                'document_date' => $node->created,
                'published' => true
            ]);
            $item->uploaded_by()->associate(User::first());

            foreach($node->countries as $country) {
               $c = Country::firstOrCreate(['name' => $country->name, 'code' => $country->name]);
               $item->countries()->attach($c);
            }


            if($node->subjects()->count() > 0) {
               $subject = $node->subjects()->first();
               $c = Subject::firstOrCreate(['name' => $subject->name]);
               $item->subject()->associate($c);
            }

            if($node->categories()->count() > 0) {
               $category = $node->categories()->first();
               $c = ItemType::firstOrCreate(['name' => $category->name]);
               $item->item_type()->associate($c);
            }
*/
            if($node->documents()->count() > 0) {
               $document = $node->documents()->first();
               if($document->body) {
                   $item->summary =  $document->body;
                }
            }

            if($node->authors()->count() > 0) {
               $authors = $node->authors()->first()->field_institutional_author_value;
               Log::info('AUTHORS:');
               foreach(preg_split("/\s*(\,|\&|and)\s+/", $authors) as $author) {
                   $author = trim($author);
                   if($author) {
                       Log::info($author);
                       $c = Author::firstOrCreate(['name' => $author]);
                       $item->authors()->attach($c);
                    }
                }
            }

            /*if($node->covers()->count() > 0) {
               Log::info('DOWNLOAD COVER:');
               $cover = $node->covers()->first();
               $file_url = $node->web.$cover->filepath;
               Log::info($file_url);
               $ext_explode = explode('.', $cover->filepath);
               $ext = $lastEl = array_values(array_slice($ext_explode, -1))[0];
               $filename = str_random(40).'.'.$ext;
               $try_count = 1;

               while($try_count <= 3) {
                   try {
                        $cover_img = file_get_contents($file_url);
                        $file_cover = Storage::disk('public')->put('upload/'.$filename, $cover_img);
                        $file_path = 'public/upload/'.$filename;
                        $file_download = 'upload/'.$filename;
                        $item_cover = ItemCover::create([
                            'path' => $file_path,
                            'filename' => $cover->filename ? $cover->filename : $filename,
                            'file_id' => "",
                            'token' => "",
                            'description' => "",
                            'filetype' => $ext,
                            'download_path' => $file_download
                        ]);

                        $item_cover->item()->associate($item);
                        $item_cover->save();

                        break;
                    } catch (Exception $e)  {
                        Log::info('Try: '.$try);
                        $try_count += 1;
                    }
                }
            }

            if($node->files()->count() > 0) {
                Log::info('DOWNLOAD FILES:');
                foreach($node->files()->orderBy('fid', 'asc')->get() as $file) {
                   $file_url = $node->web.$file->filepath;
                   Log::info($file_url);
                   $ext_explode = explode('.', $file->filepath);
                   $ext = $lastEl = array_values(array_slice($ext_explode, -1))[0];
                   $filename = str_random(40).'.'.$ext;
                   $try_count = 1;

                   while($try_count <= 3) {
                       try  {
                           $file_img = file_get_contents($file_url);
                           $file_file = Storage::disk('public')->put('upload/'.$filename, $file_img);
                           $file_path = 'public/upload/'.$filename;
                           $file_download = 'upload/'.$filename;

                           $item_file = ItemFiles::create([
                               'path' => $file_path,
                               'filename' => $file->filename ? $file->filename : $filename,
                               'file_id' => "",
                               'token' => "",
                               'description' => "",
                               'filetype' => $ext,
                               'download_path' => $file_download
                           ]);

                           $item_file->item()->associate($item);
                           $item_file->save();
                           break;
                        } catch (Exception $e)  {
                            Log::info('Try: '.$try_count);
                            $try_count += 1;
                        }
                    }
                }
            }*/

            //$item->created_at = $node->created;
            $item->save();
            Log::info('---------------------------------');
        }
    }
}
