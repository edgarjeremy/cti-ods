<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $root_role = new Role();
        $root_role->name = "root";
        $root_role->complete_name = "ROOT";
        $root_role->description = "The Almighty Role";
        $root_role->save();

        $admin_role = new Role();
        $admin_role->name = "admin";
        $admin_role->complete_name = "Admin";
        $admin_role->description = "Admin Role";
        $admin_role->save();

        $contributor_role = new Role();
        $contributor_role->name = "contributor";
        $contributor_role->complete_name = "Contributor";
        $contributor_role->description = "Contributor Role";
        $contributor_role->save();

        $febrian = User::create([
            'first_name' => 'Febrian',
            'last_name' => 'Rendak',
            'email' => 'febriananugrah92@gmail.com',
            'password' => bcrypt('cerberus1992?'),
            'biography' => '',
        ]);

        $febrian->role()->associate($root_role);
        $febrian->save();

        $cticff = User::create([
            'first_name' => 'ADMIN',
            'last_name' => 'CTICFF',
            'email' => 'admin@cticff.org',
            'password' => bcrypt('admin-admon'),
            'biography' => '',
        ]);

        $cticff->role()->associate($admin_role);
        $cticff->save();
    }
}
