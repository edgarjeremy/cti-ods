<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_author = ['Environment', 'Annual Meeting', 'Weather'];
        foreach($list_author as $name) {
            Subject::create([
                'name' => $name
            ]);
        }
    }
}
