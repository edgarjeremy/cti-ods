$(document).ready(function(){
  console.log("list_items.js");
  var url = '/admin/items'
  var regex = new RegExp(url);
  var text = window.location.href;
  var current_click = 1;
  var offset = (current_click - 1) * 5;
  text.match(regex)
  if (text.match(regex))
  {
    request_list(0);
  }

  var items = 5;

  $('.load-files').click(function(){
    current_click++;
    items += 5;
    $('#items-show').text(items);
    offset = (current_click - 1) * 5;
    request_list(offset);
  });

  function request_list(offset)
  {
    if (offset === undefined)
    {
      offset = 0;
    }
    $.ajax({
      url : "../items/5/"+ offset,
      headers : {
        'X-CSRF-TOKEN' : 'bNgExI2EfWoCFith7q5mndNoNHGqFfm9dA2CfNzN',
        'X-XSRF-TOKEN' : 'eyJpdiI6IlBRQkpuWEYzV1cwN0NnMVdZa0xqZUE9PSIsInZhbHVlIjoiOTRFM1lcL3VJNXpKR2FubUoyZVRRYXdYS3ZMU1l6Y0QrVW1TU2pqMDZYWFNYbkNYYzBJZnZ0cXJUVWdzN2VYNjBQVDRxNDlpQ0F6XC96N09SWVpUV05mQT09IiwibWFjIjoiZjczMzJkNmZmZDkyYzkzMGExNTY5Y2EyMTI4NjFjMDhhMDhhMzI5ZTM0NmZjYWVmMjFjOWFiNWRhYjgxY2M0NSJ9'
      },
      success : function(resp)
      {
        // console.log(resp);
        append_list(resp.item);
        $('#total-items').text(resp.item_total);
        $('#items-show').text(items);
      }
    })
  }

  function append_list(resp)
  {
    for (let i = 0; i < resp.length; i++)
    {
      $.ajax({
        url : "../profile_photo/" + resp[i].user_id,
        // headers : {
        //   'X-CSRF-TOKEN' : 'bNgExI2EfWoCFith7q5mndNoNHGqFfm9dA2CfNzN',
        //   'X-XSRF-TOKEN' : 'eyJpdiI6Imw3R2s5a09KQ1hEUXg3N2d6clZMcGc9PSIsInZhbHVlIjoiODZVWFVwRE9YUFZ1ZHpIdU5udGlkY3YyOHV0bHoyaW1LSERXeCs5TURiTHVLTjNubHNINFNZK0FcLytYdmJJd1hPM20rOTRZZmJXUlJLRnJGTUZYeEJ3PT0iLCJtYWMiOiI2MjdlNzA4ZDE3OTVkZmM1ZDAwNmVlMzdkMmZkOGVkNzVkOTlhNzY0ODEyY2EwODk1NjEyMjUyNWNkN2ZhN2RkIn0='
        // },
        success : function(response)
        {
          // console.log(resp[i]);
          $("#items-list").append("<li><div class='img'><img src='/ods/public/storage/"+ response[0].download_path +"'></div><a href='../book-details/" + resp[i].id + "'>" + resp[i].title + "<span> Uploaded by " + resp[i].first_name + "<label>" + moment(resp[i].updated_at).format('dddd, MMMM Do YYYY'));
        }
      })
    }
  }

  function current_user()
  {
    // $.ajax({
    //
    // })
  }

  function get_profile(id)
  {
    var path = "/ods/public/storage/";
    var img;
    $.ajax({
      url : "../profile_photo/" + id,
      headers : {
        'X-CSRF-TOKEN' : 'bNgExI2EfWoCFith7q5mndNoNHGqFfm9dA2CfNzN',
        'X-XSRF-TOKEN' : 'eyJpdiI6IlBRQkpuWEYzV1cwN0NnMVdZa0xqZUE9PSIsInZhbHVlIjoiOTRFM1lcL3VJNXpKR2FubUoyZVRRYXdYS3ZMU1l6Y0QrVW1TU2pqMDZYWFNYbkNYYzBJZnZ0cXJUVWdzN2VYNjBQVDRxNDlpQ0F6XC96N09SWVpUV05mQT09IiwibWFjIjoiZjczMzJkNmZmZDkyYzkzMGExNTY5Y2EyMTI4NjFjMDhhMDhhMzI5ZTM0NmZjYWVmMjFjOWFiNWRhYjgxY2M0NSJ9'
      },
      success : function(resp)
      {
        img = resp.download_path;
      }
    })

    return img;
  }

});
