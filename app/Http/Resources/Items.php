<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\Users;

class Items extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "alternative_title" => $this->alternative_title,
            "document_date" => $this->document_date,
            "preview" => $this->preview,
            "summary" => $this->summary,
            "user_id" => $this->user_id,
            'uploaded_by' => new Users($this->uploaded_by),
            'visibility' => new Visibility($this->visibility),
            "item_type_id" => $this->item_type_id,
            'item_type' => new ItemTypes($this->item_type),
            'subject' => new Subjects($this->subject),
            'subject_id' => $this->subject_id,
            "deleted" => $this->deleted,
            "published" => $this->published,
            "files" => new ItemFiles($this->files),
            "countries" => new Countries($this->countries),
            "authors" => new Authors($this->authors),
            "tags" => new Authors($this->tags),
            "cover" => new ItemCover($this->cover),
            "created_at" => $this->created_at ? $this->created_at->toDateTimeString() : $this->created_at,
            "updated_at" => $this->updated_at ? $this->updated_at->toDateTimeString() : $this->updated_at,
            "deleted_at" => $this->deleted_at ? $this->deleted_at->toDateTimeString() : $this->deleted_at
        ];
    }
}
