<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Visibility extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "item_id" => $this->item_id,
            "type" => $this->type,
            "users" => User::collection($this->users)
        ];
    }
}
