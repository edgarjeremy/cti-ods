<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\Resource;

class Users extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "email" => $this->email,
            "role" => new Roles($this->role),
            "full_name" => $this->full_name(),
            "current_user" => $this->id == Auth::user()->id
            //"created_at" => $this->created_at,
            //"updated_at" => $this->updated_at,
            //"role_id" => $this->role_id,
        ];
    }
}
