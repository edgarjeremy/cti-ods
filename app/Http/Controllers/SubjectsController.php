<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Subject;

class SubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', ['except' => ['index']]);
    }

    public function index()
    {
        return view('subjects.index', ['subjects' => Subject::all()]);
    }


    public function create()
    {
        return view('subjects.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $subject = Subject::create([
            'name' => $request['name'],
        ]);

        return back()->with('alert-success', 'New subject created');
    }

    public function edit($id)
    {
        $subject = Subject::findOrFail($id);
        return view('subjects.edit', ['subject' => $subject]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $subject = Subject::findOrFail($id);
        $subject->name = $request['name'];
        $subject->save();

        return back()->with('alert-success', 'Subject data saved');
    }

    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();
        return redirect()->route('subjects.index');
    }


    public function delete($id)
    {
        $subject = Subject::findOrFail($id);
        return view('subjects.delete', ['subject' => $subject]);
    }
}
