<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Country;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('countries.index', ['countries' => Country::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:3|alpha',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $country = Country::create([
            'name' => $request['name'],
            'code' => $request['code']
        ]);

        return back()->with('alert-success', 'New country created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('countries.edit', ['country' => $country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:3|alpha',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $country = Country::findOrFail($id);
        $country->name = $request['name'];
        $country->code = $request['code'];
        $country->save();

        return back()->with('alert-success', 'Country data saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();
        return redirect()->route('countries.index');
    }

    public function delete($id)
    {
        $country = Country::findOrFail($id);
        return view('countries.delete', ['country' => $country]);
    }
}
