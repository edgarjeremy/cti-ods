<?php

namespace App\Http\Controllers\API;

use Log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\ItemCover;
use App\Item;
use App\Http\Resources\ItemCover as ItemCoverResource;

class ItemCoversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemCoverResource::collection(ItemCover::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $item)
    {
        $item = Item::findOrFail($item);
        $file = $request->file('cover');
        $path = $file->path();
        $ext = $file->extension();
        $filename = $file->getClientOriginalName();

        $file_path = $file->store('public/upload');
        $download_path = explode('public/', $file_path)[1];

        $item_cover = ItemCover::create([
            'path' => $file_path,
            'filename' => $filename,
            'file_id' => "",
            'token' => "",
            'description' => "",
            'filetype' => $ext,
            'download_path' => $download_path
        ]);

        $item_cover->item()->associate($item);
        $item_cover->save();

        return new ItemCoverResource($item_cover);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ItemsResource(Item::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        $item = Item::findOrFail($item);

        if($item->cover) {
            $item->cover->delete();
        }

        $file = $request->file('cover');
        $path = $file->path();
        $ext = $file->extension();
        $filename = $file->getClientOriginalName();

        $file_path = $file->store('public/upload');
        $download_path = explode('public/', $file_path)[1];

        $item_cover = ItemCover::create([
            'path' => $file_path,
            'filename' => $filename,
            'file_id' => "",
            'token' => "",
            'description' => "",
            'filetype' => $ext,
            'download_path' => $download_path
        ]);

        $item_cover->item()->associate($item);
        $item_cover->save();

        return new ItemCoverResource($item_cover);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ItemCover::findOrFail($id);
        $item->delete();

        return response()->json([
            'status' => 'ok',
            'action' => 'delete'
        ], 200);
    }
}
