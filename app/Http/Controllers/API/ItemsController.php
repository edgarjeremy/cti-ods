<?php

namespace App\Http\Controllers\API;

use Log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Item;
use App\ItemType;
use App\ItemFiles;
use App\Country;
use App\Author;
use App\Tag;
use App\Subject;
use App\Visibility;
use App\User;
use App\LogItem;
use App\LogUserNotification;
use App\VisibilityUsers;
use App\Http\Resources\Items as ItemsResource;
use Illuminate\Support\Facades\DB;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //return ItemsResource::collection(Auth::user()->items->orderBy('created_at', 'desc'));
        return ItemsResource::collection(Auth::user()->items()->orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info($request);
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            //'alternative_title' => 'string|max:255',
            //'preview' => 'string',
            //'summary' => 'string',
            'document_date' => 'date',
            'item_type_id' => 'nullable|integer|exists:item_types,id',
            'subject_id' => 'nullable|integer|exists:subjects,id',
            //'item_files' => 'required',
            'published' => 'required',
            'countries' => 'present|array',
            'authors' => 'present|array',
            'tags' => 'present|array',
            'visibility' => 'required|integer',
            'visibility_users' => 'present|array'
        ]);

        if($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 400);
        }

        $item = Item::create([
            'title' => $request['title'],
            'alternative_title' => $request['alternative_title'],
            'preview' => $request['preview'],
            'summary' => $request['summary'],
            'document_date' => $request['document_date'],
            'published' => $request['published']
        ]);

        $item->uploaded_by()->associate(Auth::user());

        foreach($request['countries'] as $country) {
            $c = Country::find($country['value']);
            $item->countries()->attach($c);
        }

        foreach($request['authors'] as $author) {
            $c = Author::find($author['value']);
            $item->authors()->attach($c);
        }

        foreach($request['tags'] as $tag) {
            $c = Tag::firstOrCreate(['id' => $tag['value']], ['name' => $tag['label']]);
            $item->tags()->attach($c);
        }

        if($request['item_type_id']) {
            $item_type = ItemType::find($request['item_type_id']);
            $item->item_type()->associate($item_type);
        }

        if($request['subject_id']) {
            $subject = Subject::find($request['subject_id']);
            $item->subject()->associate($subject);
        }

        $visibility = $item->visibility()->create([
            'type' => $request['visibility']
        ]);

        Log::info($visibility);
        Log::info($visibility->users);

        foreach($request['visibility_users'] as $user) {
            $c = User::find($user['value']);
            $visibility->users()->attach($c);
        }
        
        $visibility->save();
        $item->save();

        $log_item = LogItem::create([
            "user_id" => Auth::user()->id,
            "item_id" => $item->id,
            "submit_action" => 1
        ]);
        $log_item->save();
        
        if($request["visibility"] != 4) {
            $userList = User::where("id", "!=", auth()->user()->id)->get();
            foreach($userList as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u->id,
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        } else {
            foreach($request["visibility_users"] as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u["value"],
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        }
        
        return response()->json($item, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $item = Auth::user()->items()->findOrFail($id);
        // return new ItemsResource($item);
        $item = Item::with("visibility.users")->findOrFail($id);
        if($item->visibility->type == 4) {
            if(!$this->is_user_allowed($item->visibility->users) && $item->user_id != auth()) {
                return abort(404);
            }
        }
        return new ItemsResource($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info($request);
        $item = Auth::user()->items()->findOrFail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            //'alternative_title' => 'string|max:255',
            //'preview' => 'string',
            //'summary' => 'string',
            'document_date' => 'date',
            'item_type_id' => 'nullable|integer|exists:item_types,id',
            'subject_id' => 'nullable|integer|exists:subjects,id',
            //'item_files' => 'required',
            'published' => 'required',
            'deleted_files' => 'present|array',
            'countries' => 'present|array',
            'authors' => 'present|array',
            'tags' => 'present|array',
            'visibility' => 'required|integer',
            'visibility_users' => 'present|array'
        ]);

        if($validator->fails()) {
            Log::info($validator->errors());
            return response()->json(['errors'=>$validator->errors()], 400);
        }

        $item->title = $request['title'];
        $item->alternative_title = $request['alternative_title'];
        $item->preview = $request['preview'];
        $item->summary = $request['summary'];
        $item->document_date = $request['document_date'];
        $item->published = $request['published'];

        if($request['item_type_id']) {
            $item_type = ItemType::find($request['item_type_id']);
            $item->item_type()->associate($item_type);
        } else {
            $item->item_type_id = null;
        }

        if($request['subject_id']) {
            $subject = Subject::find($request['subject_id']);
            $item->subject()->associate($subject);
        } else {
            $item->subject_id = null;
        }

        foreach($request['deleted_files'] as $file_id) {
            $item_file = ItemFiles::find($file_id);
            $item_file->delete();
        }

        $item->countries()->detach();

        foreach($request['countries'] as $country) {
            $c = Country::find($country['value']);
            $item->countries()->attach($c);
        }

        $item->authors()->detach();

        foreach($request['authors'] as $author) {
            $c = Author::find($author['value']);
            $item->authors()->attach($c);
        }

        $item->tags()->detach();

        foreach($request['tags'] as $tag) {
            $c = Tag::firstOrCreate(['id' => $tag['value']], ['name' => $tag['label']]);
            $item->tags()->attach($c);
        }

        $item->visibility()->delete();
        $visibility = $item->visibility()->create([
            'type' => $request['visibility']
        ]);

        Log::info($visibility);
        Log::info($visibility->users);

        foreach($request['visibility_users'] as $user) {
            $c = User::find($user['value']);
            $visibility->users()->attach($c);
        }

        $visibility->save();
        $item->save();

        
        $log_item = LogItem::create([
            "user_id" => Auth::user()->id,
            "item_id" => $item->id,
            "submit_action" => 2
        ]);
        $log_item->save();
        
        if($request["visibility"] != 4) {
            $userList = User::where("id", "!=", auth()->user()->id)->get();
            foreach($userList as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u->id,
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        } else {
            foreach($request["visibility_users"] as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u["value"],
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        }

        return new ItemsResource($item);
        //return response()->json($item, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Auth::user()->items()->with("visibility", "files")->findOrFail($id);
        $log_item = LogItem::create([
            "user_id" => Auth::user()->id,
            "item_id" => $id,
            "submit_action" => 3
        ]);
        $log_item->save();

        $userList = [];
        if($item->visibility->type != 4) {
            $userList = User::where("id", "!=", auth()->user()->id)->get();
            foreach($userList as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u->id,
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        } else {
            $userList = VisibilityUsers::where("visibility_id", $item->visibility->id)->get();
            foreach($userList as $u) {
                $log_user_notification = LogUserNotification::create([
                    "user_id" => $u->user_id,
                    "log_item_id" => $log_item->id
                ]);
                $log_user_notification->save();
            }
        }

        $item->delete();

        return response()->json([
            'status' => 'ok',
            'action' => 'delete',
            "item" => $item,
            "user_to_broadcast" => $userList
        ], 200);
    }


    /**
     * Dashboard routes
     */
    public function shared_items($limit = 10, $offset = 0) {
        $q = $_GET["q"];
        $items = DB::select("
            select i.*, it.name as category, sb.name as subject, us.first_name as uploader_first_name, us.last_name as uploader_last_name, up.download_path as uploader_photo from items i
            left join visibilities v on v.item_id = i.id
            left join visibility_users vu on vu.visibility_id = v.id
            left join users u on u.id = vu.user_id
            left join users us on us.id = i.user_id
            left join user_photos up on us.id = up.user_id
            left join item_types it on i.item_type_id = it.id
            left join subjects sb on i.subject_id = sb.id
            where (u.id = :user_id or v.type in (1,2)) 
            and i.title like '%{$q}%'
            and i.deleted_at is null
            order by i.created_at ASC
            limit :limit offset :offset
        ",["user_id" => auth()->user()->id, "limit" => $limit, "offset" => $offset]);
        $count = DB::select("
            select count(*) as aggregate from (
                select i.*, it.name as category, sb.name as subject, us.first_name as uploader_first_name, us.last_name as uploader_last_name, up.download_path as uploader_photo from items i
                left join visibilities v on v.item_id = i.id
                left join visibility_users vu on vu.visibility_id = v.id
                left join users u on u.id = vu.user_id
                left join users us on us.id = i.user_id
                left join user_photos up on us.id = up.user_id
                left join item_types it on i.item_type_id = it.id
                left join subjects sb on i.subject_id = sb.id
                where (u.id = :user_id or v.type in (1,2))
                and i.title like '%{$q}%'
                and i.deleted_at is null
                order by i.created_at ASC
            ) a;
        ",["user_id" => auth()->user()->id])[0]->aggregate;
        
        return response()->json([
            "items" => $items,
            "total" => $count
        ]);
    }

    public function trashed_items() {
        return ItemsResource::collection(Auth::user()->items()->onlyTrashed()->orderBy('created_at', 'desc')->get());
    }

    public function delete_trashed($id) {
        $item = Auth::user()->items()->onlyTrashed()->findOrFail($id);
        // return $item;
        $item->forceDelete();
        return $item;
    }

    public function restore_trashed($id) {
        $item = Auth::user()->items()->onlyTrashed()->findOrFail($id);
        $item->restore();
        return $item;
    }

    public function empty_trash() {
        $items = Auth::user()->items()->onlyTrashed();
        $items->forceDelete();
        return response()->json([
            "status" => true
        ]);
    }

    private function is_user_allowed($users) {
        return in_array(auth()->user()->id, array_map(function($user){
            return $user[0]->id;
        }, (array)$users));
    }

}
