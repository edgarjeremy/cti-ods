<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use App\ItemFiles;
use App\Http\Resources\ItemFiles as ItemFilesResource;

class ItemFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ItemFilesResource(ItemFiles::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info($request);
        $file = $request->file('qqfile');
        $path = $file->path();
        $ext = $file->extension();
        $filename = $request['qqfilename'];
        $file_id = $request['qquuid'];
        $token = $request['qquuid'];

        $file_path = $file->store('public/upload');
        $download_path = explode('public/', $file_path)[1];

        $item_file = ItemFiles::create([
            'path' => $file_path,
            'filename' => $filename,
            'file_id' => $file_id,
            'token' => $token,
            'description' => "",
            'filetype' => $ext,
            'item_id' => $request['item_id'],
            'download_path' => $download_path
        ]);

        $item_file->save();
        return response()->json(["success" => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item_file = ItemFiles::findOrFail($id);
        return new ItemFilesResource($item_file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_file = ItemFiles::findOrFail($id);
        $item_file->delete();
        return response(204);
    }
}
