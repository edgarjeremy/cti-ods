<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        // $userid = auth()->user()->id;
        // var_dump($userid);
        // $items = DB::select("
        //     select i.*, v.type from items i
        //     left join visibilities v on v.item_id = i.id
        //     left join visibility_users vu on vu.visibility_id = v.id
        //     left join users u on u.id = vu.user_id
        //     where (u.id = :user_id or v.type in (1,2)) order by i.created_at ASC;
        // ",["user_id" => auth()->user()->id]);
        // return $items;
        // return view('layouts.main');
        // return auth()->user()->id;
        // return $items;
        return view('items.index');
    }
}
