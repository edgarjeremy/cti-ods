<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemCover
 * 
 * @property int $id
 * @property string $path
 * @property string $filename
 * @property string $file_id
 * @property string $token
 * @property string $filetype
 * @property string $download_path
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 * 
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class ItemCover extends Eloquent
{
	protected $casts = [
		'item_id' => 'int'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'path',
		'filename',
		'file_id',
		'token',
		'filetype',
		'download_path',
		'description',
		'item_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}
}
