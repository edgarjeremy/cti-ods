<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tag
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $items
 *
 * @package App\Models
 */
class Tag extends Eloquent
{
	protected $fillable = [
		'name'
	];

	public function items()
	{
		return $this->belongsToMany(\App\Models\Item::class, 'item_tags')
					->withPivot('id')
					->withTimestamps();
	}
}
