<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:15:20 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TermNode
 *
 * @property int $nid
 * @property int $vid
 * @property int $tid
 *
 * @package App\Models
 */
class TermNode extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $table = 'term_node';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'nid' => 'int',
		'vid' => 'int',
		'tid' => 'int'
	];

	protected $fillable = [
		'nid'
	];
}
