<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $role_id
 * @property string $biography
 * 
 * @property \App\Models\Role $role
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @property \Illuminate\Database\Eloquent\Collection $user_photos
 * @property \Illuminate\Database\Eloquent\Collection $visibilities
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $casts = [
		'role_id' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'password',
		'remember_token',
		'role_id',
		'biography'
	];

	public function role()
	{
		return $this->belongsTo(\App\Models\Role::class);
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class);
	}

	public function user_photos()
	{
		return $this->hasMany(\App\Models\UserPhoto::class);
	}

	public function visibilities()
	{
		return $this->belongsToMany(\App\Models\Visibility::class, 'visibility_users')
					->withPivot('id')
					->withTimestamps();
	}
}
