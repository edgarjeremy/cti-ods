<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $item_countries
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $fillable = [
		'code',
		'name'
	];

	public function item_countries()
	{
		return $this->hasMany(\App\Models\ItemCountry::class);
	}
}
