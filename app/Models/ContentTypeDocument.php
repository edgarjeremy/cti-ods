<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 16 Jan 2018 14:26:38 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentTypeDocument
 * 
 * @property int $vid
 * @property int $nid
 * @property string $field_internal_comments_value
 * @property string $field_file_location_url_url
 * @property string $field_file_location_url_title
 * @property string $field_file_location_url_attributes
 * @property string $field_file_location_page_url_url
 * @property string $field_file_location_page_url_title
 * @property string $field_file_location_page_url_attributes
 * @property string $field_institutional_author_value
 *
 * @package App\Models
 */
class ContentTypeDocument extends Eloquent
{
	protected $connection = 'mysql_cticff';
	protected $table = 'content_type_document';
	protected $primaryKey = 'vid';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'vid' => 'int',
		'nid' => 'int'
	];

	protected $fillable = [
		'nid',
		'field_internal_comments_value',
		'field_file_location_url_url',
		'field_file_location_url_title',
		'field_file_location_url_attributes',
		'field_file_location_page_url_url',
		'field_file_location_page_url_title',
		'field_file_location_page_url_attributes',
		'field_institutional_author_value'
	];
}
