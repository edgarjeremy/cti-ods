<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VisibilityUser
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * @property int $visibility_id
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Visibility $visibility
 *
 * @package App\Models
 */
class VisibilityUser extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'visibility_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'visibility_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function visibility()
	{
		return $this->belongsTo(\App\Models\Visibility::class);
	}
}
