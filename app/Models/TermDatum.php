<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:16:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TermDatum
 *
 * @property int $tid
 * @property int $vid
 * @property string $name
 * @property string $description
 * @property int $weight
 *
 * @package App\Models
 */
class TermDatum extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $primaryKey = 'tid';
	public $timestamps = false;

	protected $casts = [
		'vid' => 'int',
		'weight' => 'int'
	];

	protected $fillable = [
		'vid',
		'name',
		'description',
		'weight'
	];
}
