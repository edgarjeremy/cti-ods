<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemAuthor
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 * @property int $author_id
 * 
 * @property \App\Models\Author $author
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class ItemAuthor extends Eloquent
{
	protected $casts = [
		'item_id' => 'int',
		'author_id' => 'int'
	];

	protected $fillable = [
		'item_id',
		'author_id'
	];

	public function author()
	{
		return $this->belongsTo(\App\Models\Author::class);
	}

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}
}
