<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Item
 * 
 * @property int $id
 * @property string $title
 * @property string $alternative_title
 * @property \Carbon\Carbon $document_date
 * @property string $preview
 * @property string $summary
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * @property int $item_type_id
 * @property bool $deleted
 * @property bool $published
 * @property string $deleted_at
 * @property int $subject_id
 * @property int $visibility_id
 * 
 * @property \App\Models\ItemType $item_type
 * @property \App\Models\Subject $subject
 * @property \App\Models\User $user
 * @property \App\Models\Visibility $visibility
 * @property \Illuminate\Database\Eloquent\Collection $authors
 * @property \Illuminate\Database\Eloquent\Collection $item_countries
 * @property \Illuminate\Database\Eloquent\Collection $item_covers
 * @property \Illuminate\Database\Eloquent\Collection $item_files
 * @property \Illuminate\Database\Eloquent\Collection $subjects
 * @property \Illuminate\Database\Eloquent\Collection $tags
 * @property \Illuminate\Database\Eloquent\Collection $visibilities
 *
 * @package App\Models
 */
class Item extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int',
		'item_type_id' => 'int',
		'deleted' => 'bool',
		'published' => 'bool',
		'subject_id' => 'int',
		'visibility_id' => 'int'
	];

	protected $dates = [
		'document_date'
	];

	protected $fillable = [
		'title',
		'alternative_title',
		'document_date',
		'preview',
		'summary',
		'user_id',
		'item_type_id',
		'deleted',
		'published',
		'subject_id',
		'visibility_id'
	];

	public function item_type()
	{
		return $this->belongsTo(\App\Models\ItemType::class);
	}

	public function subject()
	{
		return $this->belongsTo(\App\Models\Subject::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function visibility()
	{
		return $this->belongsTo(\App\Models\Visibility::class);
	}

	public function authors()
	{
		return $this->belongsToMany(\App\Models\Author::class, 'item_authors')
					->withPivot('id')
					->withTimestamps();
	}

	public function item_countries()
	{
		return $this->hasMany(\App\Models\ItemCountry::class);
	}

	public function item_covers()
	{
		return $this->hasMany(\App\Models\ItemCover::class);
	}

	public function item_files()
	{
		return $this->hasMany(\App\Models\ItemFile::class);
	}

	public function subjects()
	{
		return $this->belongsToMany(\App\Models\Subject::class, 'item_subjects')
					->withPivot('id')
					->withTimestamps();
	}

	public function tags()
	{
		return $this->belongsToMany(\App\Models\Tag::class, 'item_tags')
					->withPivot('id')
					->withTimestamps();
	}

	public function visibilities()
	{
		return $this->hasMany(\App\Models\Visibility::class);
	}
}
