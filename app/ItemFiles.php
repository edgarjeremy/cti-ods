<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemFiles extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'path', 'filename', 'description', 'filetype', 'file_id', 'token', 'download_path', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id', 'id');
    }

    public function delete()
    {
        Storage::move($this->path, str_replace("upload", "trash/item_files", $this->path));
        return parent::delete();
    }
}
