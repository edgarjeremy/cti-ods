<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUserNotification extends Model
{
    //
    protected $fillable = ["user_id", "log_item_id"];

    public function log_item() {
        return $this->belongsTo("App\LogItem", "log_item_id", "id");
    }
}
