<?php

namespace App;

use Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Item extends Model
{
    use SoftDeletes;

    protected $dates = ["deleted_at"];

    protected $fillable = [
        'title', 'alternative_title', 'preview', 'summary', 'document_date', 'published'
    ];


    public function delete() {
        if($this->files->count() > 0) {
            foreach($this->files as $file) {
                $file->delete();
            }
        }
        if($this->cover)
            $this->cover->delete();
        parent::delete();
    }

    public function uploaded_by()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function visibility()
    {
        return $this->hasOne('App\Visibility', 'item_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country', 'item_countries')->withTimestamps();
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'item_tags')->withTimestamps();
    }

    public function authors()
    {
        return $this->belongsToMany('App\Author', 'item_authors')->withTimestamps();
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    public function item_type()
    {
        return $this->belongsTo('App\ItemType', 'item_type_id', 'id');
    }

    public function files()
    {
        return $this->hasMany('App\ItemFiles', 'item_id', 'id');
    }

    public function cover()
    {
        return $this->hasOne('App\ItemCover', 'item_id', 'id');
    }

    public function log_items() {
        return $this->hasMany('App\LogItem', 'item_id', 'id');
    }

    public function scopeItemAndTitle($query, $item_type, $title) {
        return $query->itemType($item_type)->title($title);
    }

    public function scopeItemType($query, $item_type) {
        return $query->where('item_type_id', $item_type);
    }

    public function scopeCategory($query, $item_type_id) {
        return $query->published()->where('item_type_id', $item_type_id);
    }

    public function scopeFindSubject($query, $subject_id) {
        return $query->published()->where('subject_id', $subject_id);
    }


    public function scopeTitle($query, $title) {
        return $query->published()->where('title', 'LIKE', "%$title%");
    }

    public function scopePublished($query) {
        return $query->where('published', 1)->orderBy('created_at', 'desc');
    }

    public function scopeUserId($query)
    {
        return $query->where('user_id', auth()->user()->id);
    }

    public function scopeRelatedItem($query, $item) {
        if($item->item_type) {
            return $query->published()->itemType($item->item_type->id)->where('id', '!=', $item->id);
        } else {
            return [];
        }
    }
}
