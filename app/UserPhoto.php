<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    //
    protected $fillable = [
        'path', 'filename', 'description', 'filetype', 'file_id', 'token', 'download_path', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function delete()
    {
        Storage::delete($this->path);
        return parent::delete();
    }
}
